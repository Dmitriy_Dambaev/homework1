﻿using Dapper;
using DB1.Config;
using HW1.Entities;
using HW1.Storage;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Mapping.Attributes;
using NHibernate.Tool.hbm2ddl;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HW1
{
    internal class Program
    {
        const string connectionString = "Host=localhost;Username=Admin;Password=123456;Database=postgres";

        static void Main(string[] args)
        { 
            Create_Categories_Table();
            Create_Accounts_Table();
            Create_Advertisements_Table();

            Insert5Advertisement();
            SelectAccount();
            SelectCateg();
            SelectAdvert();

            InsertRow();
        }

        /// <summary>
        /// Создание таблицы Категории
        /// </summary>
        static void Create_Categories_Table()
        {

            using var connection = new NpgsqlConnection(connectionString);
            connection.Open();

            var sql = @"
                CREATE SEQUENCE categories_id_seq; 

                CREATE TABLE if not exists categories
                (
                    id              BIGINT                      NOT NULL    DEFAULT NEXTVAL('categories_id_seq'),
                    name            CHARACTER VARYING(255)      NOT NULL,
                    description     CHARACTER VARYING(255),

                    CONSTRAINT categories_pkey PRIMARY KEY (id)

                );

                CREATE INDEX categories_name_idx ON categories(name);
            ";

            using var cmd = new NpgsqlCommand(sql, connection);
            var affectedRowsCount = cmd.ExecuteNonQuery().ToString();
            Console.WriteLine($"Запрос на создание таблицы Categories выполнен.");
        }

        /// <summary>
        /// Создание таблицы Аккаунты
        /// </summary>
        static void Create_Accounts_Table()
        {
            using var connection = new NpgsqlConnection(connectionString);
            connection.Open();

            var sql = @"         

                CREATE SEQUENCE accounts_id_seq;  

                CREATE TABLE if not exists accounts
                (
                    id              BIGINT                      NOT NULL    DEFAULT NEXTVAL('accounts_id_seq'),
                    first_name          CHARACTER VARYING(255)      NOT NULL,
                    last_name           CHARACTER VARYING(255)      NOT NULL,
                    middle_name         CHARACTER VARYING(255),
                    email               CHARACTER VARYING(255)      NOT NULL,
  
                    CONSTRAINT accounts_pkey PRIMARY KEY (id),
                    CONSTRAINT accounts_email_unique UNIQUE (email)
                );

                CREATE INDEX accounts_last_name_idx ON accounts(last_name);
                ";

            using var cmd = new NpgsqlCommand(sql, connection);
            var affectedRowsCount = cmd.ExecuteNonQuery().ToString();
            Console.WriteLine($"Запрос на создание таблицы Accounts выполнен.");
        }

        /// <summary>
        /// Создание таблицы Объявления
        /// </summary>
        static void Create_Advertisements_Table()
        {
            using var connection = new NpgsqlConnection(connectionString);
            connection.Open();

            var sql = @"
                CREATE SEQUENCE advertisements_id_seq;

                CREATE TABLE if not exists advertisements
                (
                    id              BIGINT                      NOT NULL    DEFAULT NEXTVAL('advertisements_id_seq'),
                    categ_id        BIGINT                      NOT NULL,   
                    account_id      BIGINT                      NOT NULL,
                    price           BIGINT                      NOT NULL,                   
                    name            CHARACTER VARYING(255)      NOT NULL,        
                    description     CHARACTER VARYING(255)      NOT NULL,
                    created         TIMESTAMP WITH TIME ZONE    NOT NULL,   
  
                    CONSTRAINT advertisements_pkey PRIMARY KEY (id),
                    CONSTRAINT advertisements_fk_categ_id FOREIGN KEY (categ_id) 
                        REFERENCES categories(id) ON DELETE CASCADE,
                    CONSTRAINT advertisements_fk_account_id FOREIGN KEY (account_id) 
                        REFERENCES accounts(id) ON DELETE CASCADE
                );
                ";

            using var cmd = new NpgsqlCommand(sql, connection);

            var affectedRowsCount = cmd.ExecuteNonQuery().ToString();

            Console.WriteLine($"Запрос на создание таблицы Advertisements выполнен.");
        }

        /// <summary>
        /// Добавление 5 строчек
        /// </summary>
        static void Insert5Advertisement()
        {
            var advertisementStorage = new AdvertisementsStorage(SessionFactory);
            var account1 = new AccountsEntity()
            {
                FirstName = "Александр",
                LastName = "Викторов",
                MiddleName = "Дмитриевич",
                Email = "avd@yandex.ru"
            };
            var account2 = new AccountsEntity()
            {
                FirstName = "Алексей",
                LastName = "Владимиров",
                MiddleName = "Николаевич",
                Email = "avn@yandex.ru"
            };
            var account3 = new AccountsEntity()
            {
                FirstName = "Виктор",
                LastName = "Вадимов",
                MiddleName = "Григорьевич",
                Email = "vvg@yandex.ru"
            };
            var account4 = new AccountsEntity()
            {
                FirstName = "Владимир",
                LastName = "Закиев",
                MiddleName = "Александрович",
                Email = "vza@yandex.ru"
            };
            var account5 = new AccountsEntity()
            {
                FirstName = "Вадим",
                LastName = "Вадимов",
                MiddleName = "Дмитриевич",
                Email = "vvd@yandex.ru"
            };

            var categ1 = new CategoriesEntity()
            {
                Name = "Автозапчасти",
                Description = "Автозапчасти"
            };
            var categ2 = new CategoriesEntity()
            {
                Name = "Бытовая электроника",
                Description = "Бытовая электроника"
            };
            var categ3 = new CategoriesEntity()
            {
                Name = "Для дома и дачи",
                Description = "Для дома и дачи"
            };
            var categ4 = new CategoriesEntity()
            {
                Name = "Хобби и отдых",
                Description = "Хобби и отдых"
            };
            var categ5 = new CategoriesEntity()
            {
                Name = "Одежда, обувь, аксессуары",
                Description = "Одежда, обувь, аксессуары"
            };

            var advertisement1 = new AdvertisementsEntity()
            {
                Categ = categ5,
                Account = account1,
                Name = "Плащ",
                Description = "Новый плащ",
                Price = 100,
                Created = DateTime.Now
            };
            var advertisement2 = new AdvertisementsEntity()
            {
                Categ = categ4,
                Account = account2,
                Name = "Книга",
                Description = "Гарри потер кубок огня",
                Price = 150,
                Created = DateTime.Now
            };
            var advertisement3 = new AdvertisementsEntity()
            {
                Categ = categ3,
                Account = account3,
                Name = "Лопата",
                Description = "Новая совковая лопата",
                Price = 50,
                Created = DateTime.Now
            };
            var advertisement4 = new AdvertisementsEntity()
            {
                Categ = categ2,
                Account = account4,
                Name = "Смартфон",
                Description = "Honor 10",
                Price = 10000,
                Created = DateTime.Now
            };
            var advertisement5 = new AdvertisementsEntity()
            {
                Categ = categ1,
                Account = account5,
                Name = "Рычаг подвески",
                Description = "Toyota Crown контрактный",
                Price = 4000,
                Created = DateTime.Now
            };

            try
            {
                advertisementStorage.SaveAll(new[] { advertisement1, advertisement2, advertisement3, advertisement4, advertisement5 });
                Console.WriteLine($"Insert into account & categ & advertisement table");
            }
            catch (Exception ex) { }
        }

        /// <summary>
        /// Аккаунты
        /// </summary>
        static void SelectAccount()
        {
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    var result = session.Query<AccountsEntity>()
                        .ToList();

                    Console.WriteLine(@$"Пользователи: {string.Join(", ",
                        result.Select(it => it.LastName +" "+ it.FirstName +" "+ it.MiddleName)
                        )}");
                }
            }
        }

        /// <summary>
        /// Категории
        /// </summary>
        static void SelectCateg()
        {
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    var result = session.Query<CategoriesEntity>()
                        .ToList();

                    Console.WriteLine(@$"Категории объявлений: {"Наименование- " + string.Join("; ",
                        result.Select(it => it.Name)
                        )}");
                }
            }
        }

        /// <summary>
        /// Выбор Категории для нового объявления
        /// </summary>
        static CategoriesEntity CategAddAdvert()
        {
            CategoriesEntity categ;
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    var categories = session.Query<CategoriesEntity>()
                        .ToList();
                    Console.WriteLine(@$"Выберите номер категории: {"Номер/Наименование- " + string.Join("; ",
                        categories.Select(it => it.Id +"/"+ it.Name)
                        )}");
                    categ = session.Query<CategoriesEntity>()
                        .Where(it => it.Id == Convert.ToInt32(Console.ReadLine()))
                        .SingleOrDefault();
                }
            }
            return categ;
        }

        /// <summary>
        /// Выбор Категории для нового объявления
        /// </summary>
        static AccountsEntity AccountAddAdvert()
        {
            AccountsEntity acc;
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    var accs = session.Query<AccountsEntity>()
                        .ToList();
                    Console.WriteLine(@$"Выберите номер пользователя: {"Номер/Фамилия Имя Отчество- " + string.Join("; ",
                        accs.Select(it => it.Id + "/"+ it.LastName + " " + it.FirstName + " " + it.MiddleName)
                        )}");
                    acc = session.Query<AccountsEntity>()
                        .Where(it => it.Id == Convert.ToInt32(Console.ReadLine()))
                        .SingleOrDefault();
                }
            }
            return acc;
        }

        /// <summary>
        /// Объявления
        /// </summary>
        static void SelectAdvert()
        {
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    var result = session.Query<AdvertisementsEntity>()
                        .ToList();

                    Console.WriteLine(@$"Объявления: {"Наименование- "+string.Join("; ",
                        result.Select(it => it.Name + ", Категория- " + it.Categ.Name + ", Описание- " + it.Description + ", Цена- " + it.Price)
                        )}");
                }
            }
        }

        /// <summary>
        /// Добавить строку в таблицу
        /// </summary>
        static void InsertRow()
        {
            try
            {
                Console.WriteLine("Выберите таблицу в которую будете добавлять новую строку\n" +
                                  "1 - Аккаунт\n" +
                                  "2 - Категория\n" +
                                  "3 - Объявление\n" +
                                  "4 - Выход");
                string choice = Console.ReadLine();
                if (choice != null)
                {
                    switch (choice)
                    {
                        case "1":
                            Console.WriteLine("Добавление нового пользователя \nВведите фамилию:");
                            string ac_lastname = Console.ReadLine();
                            Console.WriteLine("Введите имя:");
                            string ac_firstname = Console.ReadLine();
                            Console.WriteLine("Введите отчество:");
                            string ac_midlename = Console.ReadLine();
                            Console.WriteLine("Введите e-mail:");
                            string ac_email = Console.ReadLine();
                            var acc = new AccountsEntity()
                            {
                                LastName = ac_lastname,
                                FirstName = ac_firstname,
                                MiddleName = ac_midlename,
                                Email = ac_email,
                            };
                            var accountStorage = new AccountStorage(SessionFactory);
                            accountStorage.Save(acc);
                            Console.WriteLine($"Добавлен новый пользователь");
                            SelectAccount();
                            break;
                        case "2":
                            Console.WriteLine("Добавление новой категории \nВведите название:");
                            string c_Name = Console.ReadLine();
                            Console.WriteLine("Введите описание:");
                            string c_Desc = Console.ReadLine();
                            var categ = new CategoriesEntity()
                            {
                                Name = c_Name,
                                Description = c_Desc
                            };
                            var categStorage = new CategoriesStorage(SessionFactory);
                            categStorage.Save(categ);
                            Console.WriteLine($"Добавлена новая категория");
                            SelectCateg();
                            break;
                        case "3":
                            Console.WriteLine("Добавление нового объявления ");
                            var categAddAdvert = CategAddAdvert();
                            var accAddAdver = AccountAddAdvert();
                            Console.WriteLine("Введите название:");
                            var nameAddAdvert = Console.ReadLine();
                            Console.WriteLine("Введите описание:");
                            var descAddAdvert = Console.ReadLine();
                            Console.WriteLine("Введите цену:");
                            var price = Convert.ToInt32(Console.ReadLine());
                            var advertisement = new AdvertisementsEntity()
                            {
                                Categ = categAddAdvert,
                                Account = accAddAdver,
                                Name = nameAddAdvert,
                                Description = descAddAdvert,
                                Price = price,
                                Created = DateTime.Now
                            };
                            var advertisementStorage = new AdvertisementsStorage(SessionFactory);
                            advertisementStorage.Save(advertisement);
                            Console.WriteLine($"Добавлено новое объявление");
                            SelectAdvert();
                            break;
                        case "4":
                            break;
                    }
                }
            }
            catch (Exception ex) { Console.WriteLine($"Ошибка добавления"); }
        }

        static ISessionFactory sessionFactory;
        static ISessionFactory SessionFactory
        {
            get
            {
                if (sessionFactory == null)
                {
                    var configProperties = new Dictionary<string, string>
                    {
                        {
                            NHibernate.Cfg.Environment.ConnectionDriver,
                            typeof (NHibernate.Driver.NpgsqlDriver).FullName
                        },
                        {
                            NHibernate.Cfg.Environment.Dialect,
                            typeof (NHibernate.Dialect.PostgreSQL82Dialect).FullName
                        },
                        {
                            NHibernate.Cfg.Environment.ConnectionString,
                            connectionString
                        },
                    };

                    var serializer = HbmSerializer.Default;
                    serializer.Validate = true;

                    var configuration = new Configuration()
                        .SetProperties(configProperties)
                        .SetNamingStrategy(new PostgresNamingStrategy())
                        .AddInputStream(serializer.Serialize(Assembly.GetExecutingAssembly()))
                        .SetInterceptor(new SqlDebugOutputInterceptor());

                    new SchemaUpdate(configuration).Execute(true, true);

                    sessionFactory = configuration.BuildSessionFactory();
                }
                return sessionFactory;
            }
        }
    }
}
