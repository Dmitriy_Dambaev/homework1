﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections.Generic;

namespace HW1.Entities
{   
    [Class(Table = "accounts")]
    public class AccountsEntity
    {
        [Id(0, Name = "Id")]
        [Generator(1, Class = "native")]
        public virtual long Id { get; set; }

        
        [Property(NotNull = true)]
        public virtual string FirstName { get; set; }

        
        [Index(0)]
        [Property(1, NotNull = true, Index = "accounts_last_name_idx")]
        public virtual string LastName { get; set; }

        
        [Property]
        public virtual string MiddleName { get; set; }


        [Property(NotNull = true, Unique = true, UniqueKey = "accounts_email_unique")]
        public virtual string Email { get; set; }

        [Bag(0, Name = "Advertisements", Inverse = true)]
        [Key(1, Column = "advertisement_id")]
        [OneToMany(2, ClassType = typeof(AdvertisementsEntity))]
        public virtual IList<AdvertisementsEntity> Advertisements { get; set; }
    }
}
