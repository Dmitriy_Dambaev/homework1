﻿using HW1.Entities;
using NHibernate;
using System.Collections.Generic;
using System.Linq;

namespace HW1.Storage
{
    class CategoriesStorage
    {
        private readonly ISessionFactory sessionFactory;

        public CategoriesStorage(ISessionFactory sessionFactory)
        {
            this.sessionFactory = sessionFactory;
        }

        public List<CategoriesEntity> GetAll()
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    return session.Query<CategoriesEntity>()
                        .Where(it => it.Name != null)
                        .ToList();
                }
            }
        }

        public void SaveOrUpdate(CategoriesEntity entity)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.SaveOrUpdate(entity);
                    tx.Commit();
                }
            }
        }

        public void Save(CategoriesEntity entity)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.Save(entity);
                    tx.Commit();
                }
            }
        }

        public void SaveAll(IEnumerable<CategoriesEntity> entities)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    foreach (var entity in entities)
                    {
                        session.Save(entity);
                    }
                    tx.Commit();
                }
            }
        }
    }
}
