﻿using HW1.Entities;
using NHibernate;
using System.Collections.Generic;
using System.Linq;

namespace HW1.Storage
{
    class AdvertisementsStorage
    {
        private readonly ISessionFactory sessionFactory;

        public AdvertisementsStorage(ISessionFactory sessionFactory)
        {
            this.sessionFactory = sessionFactory;
        }

        public List<AdvertisementsEntity> GetAll()
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    return session.Query<AdvertisementsEntity>()
                        .OrderBy(it => it.Created)
                        .ToList();
                }
            }
        }

        public void SaveOrUpdate(AdvertisementsEntity entity)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.SaveOrUpdate(entity);
                    tx.Commit();
                }
            }
        }

        public void Save(AdvertisementsEntity entity)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.Save(entity);
                    tx.Commit();
                }
            }
        }

        public void SaveAll(IEnumerable<AdvertisementsEntity> entities)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    foreach (var entity in entities)
                    {
                        session.Save(entity);
                    }
                    tx.Commit();
                }
            }
        }
    }
}
