﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections.Generic;

namespace HW1.Entities
{   
    [Class(Table = "categories")]
    public class CategoriesEntity
    {
        [Id(0, Name = "Id")]
        [Generator(1, Class = "native")]
        public virtual long Id { get; set; }


        [Index(0)]
        [Property(1, NotNull = true, Index = "categories_name_idx")]
        public virtual string Name { get; set; }


        [Property]
        public virtual string Description { get; set; }
    }
}
