﻿using NHibernate.Mapping.Attributes;
using System;

namespace HW1.Entities
{
    [Class(Table = "advertisements")]
    public class AdvertisementsEntity
    {
        [Id(0, Name = "Id")]
        [Generator(1, Class = "native")]
        public virtual long Id { get; set; }


        [ManyToOne(Column = "categ_id", ForeignKey = "advertisements_fk_categ_id", Cascade = "all")]
        public virtual CategoriesEntity Categ { get; set; }


        [ManyToOne(Column = "account_id", ForeignKey = "advertisements_fk_account_id", Cascade = "all")]
        public virtual AccountsEntity Account { get; set; }

        [Property]
        public virtual long Price { get; set; }

        [Property]
        public virtual string Name { get; set; }


        [Property]
        public virtual string Description { get; set; }


        [Property(NotNull = true)]
        public virtual DateTime Created { get; set; }
    }
}
