﻿using HW1.Entities;
using NHibernate;
using System.Collections.Generic;
using System.Linq;

namespace HW1.Storage
{
    class AccountStorage
    {
        private readonly ISessionFactory sessionFactory;

        public AccountStorage(ISessionFactory sessionFactory)
        {
            this.sessionFactory = sessionFactory;
        }

        public List<AccountsEntity> GetAllWithMiddleName()
        {
            using (ISession session = sessionFactory.OpenSession()) // unit of work
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    return session.Query<AccountsEntity>()                        
                        .Where(it => it.MiddleName != null)                            
                        .ToList();                    
                }
            }
        }

        public void SaveOrUpdate(AccountsEntity entity)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.SaveOrUpdate(entity);
                    tx.Commit();
                }
            }
        }

        public void Save(AccountsEntity entity)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.Save(entity);
                    tx.Commit();
                }
            }
        }

        public void SaveAll(IEnumerable<AccountsEntity> entities)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    foreach (var entity in entities)
                    {
                        session.Save(entity);
                    }
                    tx.Commit();
                }
            }
        }
    }
}
